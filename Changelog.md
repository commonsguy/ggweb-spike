# Changelog

## [Unreleased]

## [1.1.0]

- [Add auto-adjust volume option](https://gitlab.com/commonsguy/ggweb-spike/-/merge_requests/6)
- [Add callback for when message has been sent](https://gitlab.com/commonsguy/ggweb-spike/-/merge_requests/5)
- [Offer one-shot recording option](https://gitlab.com/commonsguy/ggweb-spike/-/merge_requests/4)
- [Apply `ktlint` and reformat code](https://gitlab.com/commonsguy/ggweb-spike/-/merge_requests/3)
- [Pass `GGWeb` instance into `onReady`](https://gitlab.com/commonsguy/ggweb-spike/-/merge_requests/2)
- [Allow location to HTML to be configurable](https://gitlab.com/commonsguy/ggweb-spike/-/merge_requests/1)
